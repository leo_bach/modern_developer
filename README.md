# Modern_Developer 2020/04/28
Using Gitlab CI/CD  
Docker  

## Lesson 1
[Architecture/Overview](https://www.youtube.com/watch?v=34u4wbeEYEo&list=PLaFCDlD-mVOlnL0f9rl3jyOHNdHU--vlJ)

```mermaid
graph TD;
  subgraph "Shared";
  C[RUNNER]:::gitlabClass;
  C---D[DOCKER]:::gitlabClass;
  end;

  subgraph "Private";
  A[RUNNER];
  A---B[DOCKER];
  end;

  subgraph "Registry";
  E[REGISTRY]:::gitlabClass;
  end;

  subgraph "High Level Architecture";
  GITLAB:::gitlabClass ==> E;
  E --- A;
  GITLAB --- C;
  GITLAB --- A;
  E --- C;
  end;

classDef gitlabClass fill:#f96;
```

Notes: Everything in orange is a property of GitLab. The grey ones are personal/local. The arrows show the relationships.  
Starts in gitlab... we have a yaml and docker file which will be passed to the personal runner or the shared runner.  
Runner is middleware that communicates (mediates) betweeen docker and gitlab.  
Build means to Compile the project.  
Deploy means to Compile the project & Publish the output.  

## Lesson 2
[Docker Download Image](https://www.youtube.com/watch?v=12AJ_WgsFYY&list=PLaFCDlD-mVOlnL0f9rl3jyOHNdHU--vlJ&index=2)
All done on Ubuntu VirtualBox
Install Docker to Your Machine
```
    ubuntu@ubuntu-VirtualBox:~$ sudo apt install docker.io
```
Help with Docker
```
    ubuntu@ubuntu-VirtualBox:~$ docker --help
```
Check Docker Images
```
    ubuntu@ubuntu-VirtualBox:~$ sudo docker images
```
Check Images Running
```
    ubuntu@ubuntu-VirtualBox:~$ sudo docker ps
```
Remove a Docker Image
```
    ubuntu@ubuntu-VirtualBox:~$ sudo docker rmi <IMAGE_ID>
```
Install an Image Example Project
```
    ubuntu@ubuntu-VirtualBox:~$ sudo docker run -dit --name my-apache-app -p 8080:80 -v "$PWD":/usr/local/apache2/htdocs/ httpd:2.4
```
Option -dit detaches the running image from our terminal (run in the background)
Option --name <NAME_TO_CALL_THIS_DOCKER_IMAGE>
Option -p <LOCAL_HOST_PORT>:<APPLICATION_PORT>
Option -v copies files in <CURRENT_WORKING_DIRECTORY>:<APPLICATION_DIRECTORY>

Inspect Docker Image Metadata
```
    ubuntu@ubuntu-VirtualBox:~$ sudo docker inspect <IMAGE_ID>
```

# Lesson 2.1
[Docker Custom Image](https://www.youtube.com/watch?v=lyDFS00Cacs&list=PLaFCDlD-mVOlnL0f9rl3jyOHNdHU--vlJ&index=3)
[Grab DockerImage](https://hub.docker.com/_/ubuntu)
```
    ubuntu@ubuntu-VirtualBox:~$ sudo docker pull ubuntu
```
Create Local Repository
```
    ubuntu@ubuntu-VirtualBox:~$ mkdir <REPOSITORY_NAME_FOR_DOCKERFILE>
```
CD to Local Repository
```
    ubuntu@ubuntu-VirtualBox:~$ cd <REPOSITORY_NAME_FOR_DOCKERFILE>
```
Create Dockerfile
```
    ubuntu@ubuntu-VirtualBox:~/dockerRepository$ touch Dockerfile
```
[Dockerfile Contents](https://gitlab.com/leo_bach/modern_developer/-/blob/master/Dockerfile)
DOCKER KEYWORKS TO UNDERSTAND:
* FROM
* RUN
* EXPOSE
* CMD

BUILD the Image
```
    ubuntu@ubuntu-VirtualBox:~/dockerRepository$ sudo docker build -t <IMAGE_NAME> .
```
This will start executing your Dockerfile:
* Sending build context to Docker daemon  2.048kB
* Step 1/6 : FROM ubuntu:latest
 ---> 1d622ef86b13
* Step 2/6 : ENV DEBIAN_FRONTEND=noninteractives
 ---> Running in ff8852a41eb2
Removing intermediate container ff8852a41eb2
 ---> 68ed131a22b6
* Step 3/6 : RUN apt-get -y update
 ---> Running in d2237d0222ee
* Step 4/6 : RUN apt-get -y install apache2
 ---> Running in df3b94d93498
* Step 5/6 : EXPOSE 80
 ---> Running in 5413644f2157
Removing intermediate container 5413644f2157
 ---> adee7ce2fa75
* Step 6/6 : CMD apachectl -D FOREGROUND
 ---> Running in 363110d7b50a
Removing intermediate container 363110d7b50a
 ---> e06e2b8e1d28
* Successfully built e06e2b8e1d28
* Successfully tagged custom_image:latest

Check Successful Build
```
    ubuntu@ubuntu-VirtualBox:~/Development/gitlabCICD_tutorial/CustomImage$ sudo docker images
        REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
        custom_image        latest              e06e2b8e1d28        3 minutes ago       208MB
        ubuntu              latest              1d622ef86b13        5 days ago          73.9MB
```

Run Docker Image
```
    ubuntu@ubuntu-VirtualBox:~/Development/gitlabCICD_tutorial/CustomImage$ sudo docker run -dit -p 80:80 custom_image
```

Verify Docker Image is Running
```
    ubuntu@ubuntu-VirtualBox:~/Development/gitlabCICD_tutorial/CustomImage$ sudo docker ps
    CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
    35befe137c46        custom_image        "/bin/sh -c 'apachec…"   11 seconds ago      Up 8 seconds        0.0.0.0:80->80/tcp   exciting_kalam
```
Verify Apache Server is Running Open Web Browser:
[Sucessfully Running Apache Server](https://gitlab.com/leo_bach/modern_developer/-/blob/master/Screenshot_from_2020-04-28_22-10-35.png)  

# Lesson 3
[Gitlab Runner](https://www.youtube.com/watch?v=fMdIH-L5Uvg&list=PLaFCDlD-mVOlnL0f9rl3jyOHNdHU--vlJ&index=4)

```mermaid
graph TD;
  subgraph "Shared";
  C[RUNNER]:::gitlabClass;
  C---D[DOCKER]:::gitlabClass;
  end;

  subgraph "Private";
  A[RUNNER];
  A---B[DOCKER];
  end;

  subgraph "Registry";
  E[REGISTRY]:::gitlabClass;
  end;

  subgraph "High Level Architecture";
  GITLAB:::gitlabClass ==> E;
  E --- A;
  GITLAB --- C;
  GITLAB --- A;
  E --- C;
  end;

classDef gitlabClass fill:#f96;
```

Gitlab Runner and Docker need a way to communicate bidirectionally. Enter: Gitlab Runner

Create a New Project in Gitlab
Go to Settings
Go to CI/CD
Select Runners "EXPAND"

Install Gitlab Runner
```
    ubuntu@ubuntu-VirtualBox:~$ wget https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
    ubuntu@ubuntu-VirtualBox:~$ apt install ./gitlab-runner_amd64.deb
```
[Version Problems if Using UBUNTU 19+](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4773)

Check Gitlab Runner
```
    ubuntu@ubuntu-VirtualBox:~$ sudo gitlab-runner --version
```

Register a Runner
```
    ubuntu@ubuntu-VirtualBox:~/Development/gitlabCICD_tutorial$ sudo gitlab-runner register
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
    https://gitlab.com/
Please enter the gitlab-ci token for this runner:
    <TOKEN_FROM_GITLAB_WEBGUI_SETUP_SPECIFIC_RUNNER_MANUALLY>
Please enter the gitlab-ci description for this runner:
    [ubuntu-VirtualBox]: this is a gitlab ci-cd tutorial
Please enter the gitlab-ci tags for this runner (comma separated):
    <TAGS_SHOULD_MATCH_THOSE_STAGES_IN_OUR_YAML_FILE_ON_GITHUB>
    stage,qa,build,deploy
Registering runner... succeeded                     runner=g2_2WyzE
Please enter the executor: shell, ssh, docker+machine, custom, parallels, virtualbox, docker-ssh+machine, kubernetes, docker, docker-ssh:
    shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

# Lesson 4
[Gitlab Registry](https://www.youtube.com/watch?v=O5sbhykF-q4&list=PLaFCDlD-mVOlnL0f9rl3jyOHNdHU--vlJ&index=5)