FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractives
RUN apt-get -y update
RUN apt-get -y install apache2
EXPOSE 80
CMD apachectl -D FOREGROUND